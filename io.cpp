#include "io.h"
#include <iostream>
using namespace std;

int readNumber() {
	int number = 0;
	cout << "Enter number ";
	cin >> number;
	return number;
}

void writeAnswer(int firstNumber, int secondNumber) {
	cout << "Sum: " << firstNumber + secondNumber;
}